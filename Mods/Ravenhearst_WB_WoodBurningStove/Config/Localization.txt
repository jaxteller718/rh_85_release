﻿Key,Source,Context,English
StoveRepairKit,Items,Item,Wood Burning Stove Repair Kit
StoveRepairKitDesc,Items,Item,"Use it to repair old Wood Burning Stove and make it usable."
TOONStoveOld,Blocks,Block,Old Working Stove
TOONStoveOldDesc,Blocks,Block,"An old Wood Burning Stove to cook Food and some Chemicals at home."